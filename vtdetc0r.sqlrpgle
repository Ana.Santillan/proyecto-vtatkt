**free
ctl-opt main(main);

dcl-f vtdetc0w workstn sfile(s01:nrr) alias usropn;

dcl-pr vtdetm0r extpgm;
    parm_modo    char(1) const;
    parm_nro_doc packed(8:0) const;
end-pr;


dcl-s nrr packed(4:0) inz(*zero);

dcl-ds C1 extname('VTDE') alias end-ds;

dcl-proc main;

    dcl-s srcstr        varchar(52);

    
    exsr inicializar_programa;

    exfmt P01;
    dow *in03 =  *off;
        exsr trabajar_con_result_set;        
        exfmt P01;
    enddo;

    exsr finalizar_programa;

    //-------------------------------------------------
    begsr inicializar_programa;
    //-------------------------------------------------
        open vtdetc0w;
    endsr;
    
    //-------------------------------------------------
    begsr finalizar_programa;
    //-------------------------------------------------
        close vtdetc0w;     
        return;
    endsr;
    
    //-------------------------------------------------
    begsr trabajar_con_result_set;
    //-------------------------------------------------
        exsr mostrar_result_set;
        dow *in12 = *off;
            exsr trabajar_con_selecciones;
            exsr mostrar_result_set;
        enddo;
    endsr;
    //-------------------------------------------------
    begsr trabajar_con_selecciones;
    //-------------------------------------------------
        if *in06;
        vtdetm0r('6':*zero);
        leavesr;
        endif;

        readc s01;
        dow not %eof();
            vtdetm0r(opc:NRO_DOCUMENTO);
            readc s01;
        enddo;

    endsr;



    //-------------------------------------------------
    begsr mostrar_result_set;
    //-------------------------------------------------

        *in30 = *off;
        *in31 = *off;
        *in40 = *off;
        nrr = *zero;

        write c01;

        exsr abrir_cursor_lineal;
        exsr fetch_next;
        dow sqlcode = *zero and nrr < 99;
            nrr = nrr + 1;
            write s01;
            exsr fetch_next;
        enddo;     
        exsr cerrar_cursor_lineal;

        if nrr >= 99;
            *in40 = *on;
        endif;

        *in31=*on;
        if nrr > 0;
           *in30 = *on;
        endif;

        write f01;
        exfmt c01;

    endsr;

    //-------------------------------------------------
    begsr abrir_cursor_lineal;
    //-------------------------------------------------
        
        srcstr = '%'+%trim(SRCNME)+'%';

        exec sql DECLARE C1 CURSOR FOR
            SELECT
                *
            FROM VTDE
            WHERE 
                    NOMBRE_EMPLEADO LIKE :SRCSTR
            OR      CAST(NRO_DOCUMENTO AS CHAR(8))  LIKE :SRCSTR
        ;

        exec sql OPEN C1;

    endsr;

    //-------------------------------------------------
    begsr fetch_next;
    //-------------------------------------------------
        
        exec sql FETCH C1 INTO :C1;

    endsr;

    //-------------------------------------------------
    begsr cerrar_cursor_lineal;
    //-------------------------------------------------
        exec sql CLOSE C1;
    endsr;

end-proc;