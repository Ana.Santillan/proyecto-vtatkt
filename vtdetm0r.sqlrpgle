**free
ctl-opt main(main) dftactgrp(*no);

dcl-f vtdetm0w workstn  alias usropn;

dcl-ds VTDE extname('VTDE')  alias end-ds;
dcl-ds VTCE extname('VTCE')  alias qualified end-ds;
dcl-ds VTCC extname('VTCC')  alias end-ds;

dcl-pr recuperar_texto_error_sql extpgm('NPUTSQER');
    nroerr          packed(4:0);
    sqletx          char(50);
end-pr;

dcl-proc main;
    dcl-pi main;
        parm_modo   char(1);
        parm_nrodoc packed(8:0);
        parm_identr packed(7:0);
    end-pi;

    
    dcl-s valid ind;
    dcl-s errnbr packed(4:0);
    dcl-s errdsc char(50);
    dcl-s noment char(50);

    open vtdetm0w;
    select;
        when parm_modo = '2';
            noment = 'RANCHO';
            exsr agregar_entrada;
        when parm_modo = '3';
            noment = 'VIP';
            exsr agregar_entrada;
        when parm_modo = '4';
            noment = 'PALCO';
            exsr agregar_entrada;
        when parm_modo = '6';
            exsr agregar_cuota;


    endsl;

    exsr finalizar_programa;

    //------------------------------------------------------------
    begsr finalizar_programa;
    //------------------------------------------------------------
        //Cerrar recursos etc etc 
        close vtdetm0w;
        
        exec sql commit;

        return;

    endsr;

    //------------------------------------------------------------
    begsr agregar_cuota;
    //------------------------------------------------------------
        
        valid = *off;
        *in40 = *off;
        title='Agregar Cuota';
        exfmt P02;
        dow *in12 = *off and valid = *off;
            exsr validar_registro;
            if valid;
                exsr insert_record;
                
                if sqlcode =  *zero;
                    leave;
                else;
                    errnbr=sqlcode;
                    recuperar_texto_error_sql(errnbr : errdsc);
                    msgtxt='Error al insertar:(' + %editc(sqlcod:'J') + ') '+ errdsc;
                    exfmt msgbox;
                endif;

            endif;
            exfmt P02;
        enddo;                                         
    
    endsr;

        //-------------------------------------------------------------
    begsr insert_record;
    //-------------------------------------------------------------
        exec sql
        INSERT INTO VTCC (
                DNI_COMPRADOR, 
                NRO_ENTRADA,        
                NRO_CUOTA, 
                FECHA_CUOTA, 
                IMPORTE_CUOTA
            )VALUES(
                :VTCE.DOC_COMPRADOR, 
                :VTCE.ID_ENTRADA,        
                :NRO_CUOTA, 
                :FECHA_CUOTA, 
                :IMPORTE_CUOTA             
            );
            DNI_COMPRADOR = parm_nrodoc;
            NRO_ENTRADA = parm_identr;
        // INSERT INTO VT5850D.VTCC (
        //     DNI_COMPRADOR
        //     , ID_CUOTA
        //     , NRO_ENTRADA
        //     , NRO_CUOTA
        //     , FECHA_CUOTA
        //     , IMPORTE_CUOTA)

        //     SELECT 
        //     DOC_COMPRADOR
        //     , ID_ENTRADA
        //     , 0
        //     , 0
        //     , CURRENT DATE
        //     , VALOR_ENTRADA
        //     FROM VT5850D.VTCE
        //     WHERE ID_ENTRADA = :parm_identr
        //     AND DOC_COMPRADOR = :parm_nrodoc;
            
    endsr;

    //------------------------------------------------------------
    begsr agregar_entrada;
    //------------------------------------------------------------
        
        valid = *off;
        *in40 = *off;
        title='Seleccionar Entrada';
        exsr leer_datos;
        exfmt P01;
        dow *in12 = *off and valid = *off;
            exsr validar_registro;
            if valid;
                exsr update_record;
                leave;
            endif;
            exfmt p01;
        enddo;                                         
    endsr;

    //-------------------------------------------------------------
    begsr update_record;
    //-------------------------------------------------------------
        exec sql
            UPDATE VTCE  SET
                DOC_COMPRADOR = :parm_nrodoc
                ,HORA_COMPRA     = (select current_time from sysibm.sysdummy1)
                ,FECHA_COMPRA        = (select current_date from sysibm.sysdummy1)
                ,DESCRIPCION_ENTRADA    = 'Comprada'
            WHERE
                ID_ENTRADA = :ID_ENTRADA
        ;
        
    endsr;
    

    //------------------------------------------------------------
    begsr validar_registro;
    //------------------------------------------------------------
        valid = *on;

    endsr;

    //------------------------------------------------------------
    begsr registro_visualizar;
    //------------------------------------------------------------
        title = 'Visualizar Registro';
        exsr leer_datos;
        *in40 = *on;
        exfmt p01;               
    endsr;


    //------------------------------------------------------------
    begsr leer_datos;
    //------------------------------------------------------------
        exec sql 
                SELECT  
                    
                    ID_ENTRADA      
                    ,VALOR_ENTRADA   
                    ,IFNULL(DOC_COMPRADOR, 0)
                    ,IFNULL(HORA_COMPRA, '00:00:00')     
                    ,IFNULL(FECHA_COMPRA, '2001-01-01')   
                    ,DESCRIPCION_ENTRADA 
                    INTO
                    :VTCE.ID_ENTRADA      
                    ,:VTCE.VALOR_ENTRADA   
                    ,:VTCE.DOC_COMPRADOR   
                    ,:VTCE.HORA_COMPRA     
                    ,:VTCE.FECHA_COMPRA    
                    ,:VTCE.DESCRIPCION_ENTRADA                     
                    
                FROM VTCE
                WHERE
                        ID_ENTRADA = ( SELECT
                                        ID_ENTRADA
                                    FROM VTCE
                                    WHERE
                                                (
                                                    DOC_COMPRADOR IS NULL
                                                    OR DOC_COMPRADOR = 0
                                                )
                                            AND NOMBRE_ENTRADA = :noment
                                        LIMIT 1
                                        )
                        ;
    NRO_DOCUMENTO = parm_nrodoc;
    NOMBRE_ENTRADA = noment;
    ID_ENTRADA = vtce.ID_ENTRADA;
    VALOR_ENTRADA = vtce.VALOR_ENTRADA;
    DESCRIPCION_ENTRADA = vtce.DESCRIPCION_ENTRADA;
    endsr;

end-proc;