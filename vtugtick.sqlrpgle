**free
ctl-opt main(main)  DFTACTGRP(*NO);

// dcl-s apellidos   varchar(20)  dim(10) ctdata perrcd(1);
// dcl-s nombres     varchar(20)  dim(10) ctdata perrcd(1);

dcl-proc main;


    dcl-s NOMBRE_ENTRADA        varchar(50);
    dcl-s ID_ENTRADA            packed(7:0);
    dcl-s VALOR_ENTRADA         packed(4:0);
    dcl-s DOC_COMPRADOR         packed(8:0);
    dcl-s HORA_COMPRA           time;
    dcl-s FECHA_COMPRA          date;
    dcl-s DESCRIPCION_ENTRADA   varchar(50);

    dcl-s i  packed(8:0);

    exsr limpiar_tabla;
//1000 ranchos 3000 - 300 vip 5000 - 200 palcos 7500
    for i = 1 to 1000;
        ID_ENTRADA      = generar_id_entrada();                
        exsr insertarR_registro;
    endfor;

    for i = 1 to 300;
        ID_ENTRADA      = generar_id_entrada();                
        exsr insertarV_registro;
    endfor;
    
    for i = 1 to 200;
        ID_ENTRADA      = generar_id_entrada();                
        exsr insertarP_registro;
    endfor;

    exec sql commit;
    
    return ;
    
    //---------------------------------------------------------
    begsr limpiar_tabla;
    //---------------------------------------------------------
        exec sql DELETE FROM VT5850D.VTCE;
    endsr;

    //---------------------------------------------------------
    begsr insertarR_registro;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5850D.VTCE (
                NOMBRE_ENTRADA, 
                ID_ENTRADA,       
                VALOR_ENTRADA,
                DOC_COMPRADOR,
                DESCRIPCION_ENTRADA
                ) 
                VALUES
                (   
                    'RANCHO'
                    , :ID_ENTRADA 
                    , 3000
                    , :DOC_COMPRADOR
                    , 'DISPONIBLE'
                );                         
    endsr;

    //---------------------------------------------------------
    begsr insertarV_registro;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5850D.VTCE (
                NOMBRE_ENTRADA, 
                ID_ENTRADA,       
                VALOR_ENTRADA,
                DOC_COMPRADOR, 
                DESCRIPCION_ENTRADA
                ) 
                VALUES
                (   
                    'VIP'
                    , :ID_ENTRADA 
                    , 5000
                    , :DOC_COMPRADOR
                    , 'DISPONIBLE'
                );                         
    endsr;

        //---------------------------------------------------------
    begsr insertarP_registro;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5850D.VTCE (
                NOMBRE_ENTRADA, 
                ID_ENTRADA,       
                VALOR_ENTRADA,
                DOC_COMPRADOR, 
                DESCRIPCION_ENTRADA
                ) 
                VALUES
                (   
                    'PALCO'
                    , :ID_ENTRADA 
                    , 7500
                    , :DOC_COMPRADOR
                    , 'DISPONIBLE'
                );                         
    endsr;

end-proc;

//==========================================================================
dcl-proc generar_id_entrada;
//==========================================================================
    dcl-pi *n packed(7:0);
    end-pi;

    dcl-s identr packed(7:0);
    dcl-s existe packed(7:0);

    dow *on;
        exec sql    SELECT 
                    INT((RANDOM()*9998)+1)  INTO :identr
                FROM sysibm.sysdummy1;
        
        exec sql    SELECT  
                            COUNT(*) INTO :existe
                        FROM VT5850D.VTCE
                        WHERE
                            ID_ENTRADA = : identr;
            if existe = 0 ;
                leave;
            endif;
        enddo;

        return identr;
end-proc;