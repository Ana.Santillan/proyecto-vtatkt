CREATE TABLE VT5850D.VTCE (

	NOMBRE_ENTRADA      FOR NOMENT   		  CHAR      (50)  	  NOT NULL WITH DEFAULT, 
    ID_ENTRADA          FOR IDENTR 	          DEC       (7, 0)	  NOT NULL WITH DEFAULT, 
    VALOR_ENTRADA       FOR VLRENT 	          DEC       (4, 0)	  NOT NULL WITH DEFAULT,        
    DOC_COMPRADOR       FOR DOCOMP 	          DEC       (8, 0)                         ,        
    HORA_COMPRA         FOR HORENT 	          TIME	                                   ,        
    FECHA_COMPRA        FOR FECENT 	          DATE	                                   ,        
    DESCRIPCION_ENTRADA FOR DSCENT            CHAR      (50)  	  NOT NULL WITH DEFAULT,

    PRIMARY KEY (ID_ENTRADA)
);    

LABEL ON TABLE VT5850D.VTCE IS                    
'VENTA DE TICKET-CATEGORIAS DE ENTRADAS'; 