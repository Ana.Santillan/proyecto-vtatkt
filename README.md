
# NOVPER - Sistema de Novedades de Personal


## ENUNCIADO

Disponemos de una tabla provista por la municipalidad en la cual figuran
5.000 empleados , cada uno de ellos con un "Límite" o "Margén" de dinero
que pueden gastar mediante el sistema de descuento en cuenta sueldo.
Ej.
                DNI             NOMBRE                  LIMITE          SALDO
                20222643822     OTTONELLO SANTIAGO      20.0000         15.000

(Nota: Hay que generar esta base, usando como ejemplo el programa VTUTGEMP)

Asimismo tenemos de un archivo de entradas disponibles para vender
Estas entradas pueden ser de tres categorías, a saber:

* VIP Precio 5.000
* PALCO: Precio 7.500
* Rancho: Precio 3.000

El banco dispone de 1500 entradas para  la venta, compuestas por
1000 Ranchos
300  Vip
200  Palcos
(Nota: Hay que generar esta base, usando como ejemplo el programa VTUTGEMP)

Se debe disponer de un programa que recibiendo nro de documento o nombre
permita seleccionar una persona, una vez seleccionada esta persona
se debe poder elegir el tipo de entrada que desea adquirir (VIP, PALCO o RANCHO)
y si le alcanza el saldo , asignar en la tabla de entradas el DNI de esta perona
la fecha y hora de compra.
Se debe descontar la compra de la tabla con limites de manera de reducir el saldo
luego de cada compra.
Si la persona desea adquirir una entrada para la cual no le alcanzar el saldo
mostralo como mensaje.
Se debe pedir la cantidad de cuotas en las cuales pretende pagar y generar
un archivo de cuotas mensuales que contenga
                DNI     NROENTRADA      NRO DE CUOTA    IMPORTE DE CUOTA

Debe existir un programa que muestra las entradas ya vendidas y que permita
seleccionar una para ser "reversada" es decir sacarle la marca de vendida
poniendo de esta manera la entrada de nuevo en disponibilidad de venta
, DEVOLVIENDO el saldo a la tabla de personal Y ELIMINAR las cuotas
del archivo de cuotas

Debemos tener un programa que saque una planilla (.csv) con las ventas del 
día y las deje en /home/{usuario}

Debemos tener ademas un programa que saque una planilla (.csv) las cuotas
de un mes dados , por ejemplo al pasar 04 como parámetro el programa
debe generar un CSV con las cuotas del mes 04 (Si la hubiese)

Por último hay que generar un planilla (.csv) con las entradas NO
vendidas para rendir a Ticketel.

## Resumen ejecutivo.

(40 líneas máx que cuenta de que se trata esto)


## Objetivos y limites
 
(Que queremos lograr, que está incluido y que __NO__ lo está)

## Objetivos y limites
 
(Que queremos lograr, que está incluido y que __NO__ lo está)

### Objetivos
(Que queremos lograr)

### Límites
(Que se incluye y que no)


## Ciclo operativo del Sistema.
(Describir todo el ciclo de vida del sistema en la menor cantidad de líneas posibles)

## Entidades 
(Describir por nombre las tablas y los campos destacados de las mismas, por campos destacados
se entiene campos que condicionan el flujo de trbajo de la aplicación)

## Reglas de negocios
(Condiciones que deben cumplirse en el ciclo de vida del sistema)

## Reglas de nombres.
(Reglas para nombrar los objetos)

NORMAS DE NOMBRES

ARCHIVOS FÍSICOS Y LÓGICOS

        VTxxnn	Donde:


        VT	: Fijo indica el sistema al que pertenece el archivo
        xx	: Dos letras que indican el contenido de la tabla ej. MP Maestro de Personal
        nn	: Dos números, comenzando en 01 si y solo si el archivo es un lógico

        Ejemplo:
                        VTMP01:	Maestro de Personal-MP-Por Legajo	

PROGRAMAS

	NPffccnt	Donde:

	VT	: Fijo indica el sistema al que pertenece el programa
	ff	: Letras que indican el archivo principal sobre el que trabaja el programa.
	cc	: Clase o modelo de programa de acuerdo  a la siguiente lista:
			AA	: Alta
			BB	: Consulta
			CC	: Consulta
			MM	: Mantenimiento
			TC	: Trabajar con
			TM	: Trabajar con Mantenimiento
			LS	: Listado
			SF	: Subfile sencillo
                  MM    : Mantenimiento de Maestro
			VS	: Ventana de Selección  
			nn	: Númerico si no es de un tipo especial

	n	: Número de programa si existe más de una instancia de esa clase de programas.
	t	: Indica el tipo del fuente de acuerdo a la siguiente lista:
			R	: RPG
			C	: CLP
			W	: WORKSTATION
			P	: PRINT

	
	Por ejemplo:

		NPMPTC0R	: Novedes de personal Maestro de Personas Trabajar con primer programa tipo RPG
		NPMPTC0W	: Novedes de personal Maestro de Personas Trabajar con primer programa tipo DSPF




## Programas y procedimientos
(Una lista de programas y/o tareas, con una descripción de lo que deben hacer, 
