pgm

             DCL        VAR(&QRYSTR) TYPE(*CHAR) LEN(4096)
             DCL        VAR(&FULPAT) TYPE(*CHAR) LEN( 255)
             DCL        VAR(&USRNME) TYPE(*CHAR) LEN(  10)
             DCL        VAR(&YEAR) TYPE(*CHAR) LEN(2)
             DCL        VAR(&MES) TYPE(*CHAR) LEN(2)
             DCL        VAR(&DAY) TYPE(*CHAR) LEN(2)
             DCL        VAR(&TIME) TYPE(*CHAR) LEN(9)

             RTVJOBA    USER(&USRNME)
             RTVSYSVAL SYSVAL(QYEAR) RTNVAR(&YEAR)
             RTVSYSVAL  SYSVAL(QMONTH) RTNVAR(&MES)
             RTVSYSVAL SYSVAL(QDAY) RTNVAR(&DAY)
             RTVSYSVAL SYSVAL(QTIME) RTNVAR(&TIME)
             /* RTVSYSVAL */

             DLTF QTEMP/VENTAS
             MONMSG CPF0000 EXEC(DO)
              /*CODIGO DEL CATCH*/
             ENDDO
             /* /home/novper/novvenc_u05852_20230301_125900.csv */
             MKDIR      DIR('/home/U05850/')
             MONMSG CPF0000

             CHGVAR     VAR(&FULPAT) VALUE('/home/U05850/' |< +
                          'VENTASE_' |< &USRNME |< '_' |< &YEAR |< +
                          &MES |< &DAY |< '_' |< &TIME |< '.CSV')


             CHGVAR     VAR(&QRYSTR) VALUE('CREATE TABLE +
                          QTEMP/VENTAS AS(              SELECT +
                          NOMBRE_ENTRADA, ID_ENTRADA, +
                          VALOR_ENTRADA, DOC_COMPRADOR, +
                          HORA_COMPRA,   FECHA_COMPRA, +
                          DESCRIPCION_ENTRADA FROM VTCE WHERE +
                          DESCRIPCION_ENTRADA = +
                          ''Comprada''                                -
                                                 ) WITH DATA')

             RUNSQL     SQL(&QRYSTR) COMMIT(*NONE)
             MONMSG     MSGID(CPF0000) EXEC(DO)
                GOTO ENDPGM
             ENDDO

             CPYTOIMPF  FROMFILE(QTEMP/NOVVENC) TOSTMF(&FULPAT) +
                          MBROPT(*REPLACE) STMFCCSID(*PCASCII) +
                          RCDDLM(*CRLF) FLDDLM(';') ADDCOLNAM(*SQL)

             DLTF       FILE(QTEMP/VENTAS)
             MONMSG CPF0000 EXEC(DO)
              /*CODIGO DEL CATCH*/
             ENDDO



 ENDPGM:     ENDPGM
